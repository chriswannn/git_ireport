#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;


public class IssueCreator : EditorWindow
{
    private static string id = "";
    private string tmp = "";
    
    private string issueTitle = "";
    private string description = "";
    private bool debug = false;


    
    
    private static EditorWindow _window;

    private static string path;
    private static BinaryFormatter encoderdecoder = new BinaryFormatter();
    private static FileStream fs;

    [MenuItem("Tools/Create Git Issue")]

    public static void ShowWindow()
    {
        _window = GetWindow(typeof(IssueCreator));
        path = Application.persistentDataPath + "/tc.dat";
    }

    void OnGUI()
    {

        if (CheckToken())
        {
            debug = EditorGUILayout.Toggle("Debug", debug);
            issueTitle = EditorGUILayout.TextField("Title : ", issueTitle);
            EditorGUILayout.LabelField("Description : ");
            description = EditorGUILayout.TextArea(description, GUILayout.Height(100f));
            if (CheckString(issueTitle) && CheckString(description) && GUILayout.Button("Create Issue"))
            {
                CreateIssue(issueTitle, description, debug);
            }

            if (GUILayout.Button("Delete Token"))
            {
                DeleteToken();
            }
        }
        else
        {
            tmp = EditorGUILayout.TextField("Token : ", tmp);
            id = EditorGUILayout.TextField("id : ", id);
            if (CheckString(tmp) && CheckString(id) && GUILayout.Button("Create Token & id Cache"))
            {
                SaveToken(tmp+"/"+id);
                tmp = "";
                id = "";
            }
        }
        


    }

    private bool CheckToken()
    {
        if (File.Exists(path))
        {
            return true;
        }

        return false;
    }

    private void DeleteToken()
    {
        File.Delete(path);
    }

    private void SaveToken(string tk)
    {
        fs = File.OpenWrite(path);
        encoderdecoder.Serialize(fs, tk);
        fs.Close();
    }

    private static string RetrieveToken(out string id)
    {
        id = "";
        string tk = "";
        if (File.Exists(path))
        {
            fs = File.OpenRead(path);
            string data = (string)encoderdecoder.Deserialize(fs);
            tk = DataCutter(data, out id);
            fs.Close();
            return tk;
        }
        return null;
    }

    private static string DataCutter(string data, out string id)
    {
        id = "";
        bool sliced = false;
        string tk = "";
        for (int i = 0; i < data.Length; i++)
        {
            if (!sliced)
            {
                if (data[i] == '/')
                {
                    sliced = true;
                }
                else
                {
                    tk += data[i];
                }
            }
            else
            {
                id += data[i];
            }
        }

        return tk;
    }


    private bool CheckString(string str)
    {
        for (int i = 0; i < str.Length; i++)
        {
            if (str[i] != ' ')
            {
                return true;
            }
        }

        return false;
    }
    public static async void CreateIssue(string title, string description, bool debug)
    {
        string tk = "";
        title = title.Trim();
        using (var httpClient = new HttpClient())
        {
            if ((tk = RetrieveToken(out id)) == null)
            {
                return;
            }
            using (var request = new HttpRequestMessage(new HttpMethod("POST"), $"https://gitlab.com/api/v4/projects/{id}/issues?title={title}&description={description}"))
            {

                request.Headers.TryAddWithoutValidation("PRIVATE-TOKEN", tk);
                var response = await httpClient.SendAsync(request);
                if (debug) Debug.Log(response);

            }
        }

    }
    
    

/*
    public static void CreateIssue(string issue)
    {
        WebRequest request = WebRequest.Create ("https://gitlab.com/chriswannn/pokehome/-/issues");
        request.Method = "POST";
        string postData = $"{{'title':'{issue}', 'id':'32951099'}}";
        byte[] byteArray = Encoding.UTF8.GetBytes (postData);
        request.Headers["PRIVATE-TOKEN"] = tk;
        request.ContentLength = byteArray.Length;
        Stream dataStream = request.GetRequestStream ();
        dataStream.Write (byteArray, 0, byteArray.Length);
        dataStream.Close ();
        WebResponse response = request.GetResponse ();
        Debug.Log(response);
    }*/

}


#endif
